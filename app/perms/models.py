from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from .permissions import (secretary_view_report_by_country,
                          secretary_change_report_by_country,
                          commissar_view_report)


class Org(models.Model):
    COUNTRIES = (
        ('ro', _("Romania")),
        ('de', _("Germany")),
        ('be', _("Belgium"))
    )
    name = models.CharField(max_length=100, default='n/a',
                            verbose_name=_("Organisation name"),
                            help_text=_("Organisation name"))
    country = models.CharField(max_length=100, default='ro', choices=COUNTRIES,
                               verbose_name=_("Organisation country"),
                               help_text=_("Organisation country"))
    added = models.DateTimeField(auto_now_add=True, editable=False, null=True,
                                 verbose_name=_("Added date"),
                                 help_text=_("Added date"))

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'perms'
        ordering = ('name',)
        verbose_name = _("organisation")
        verbose_name_plural = _("organisations")


class Obligation(models.Model):
    name = models.CharField(max_length=100, default='n/a',
                            verbose_name=_("Organisation obligation"),
                            help_text=_("Organisation obligation"))
    added = models.DateTimeField(auto_now_add=True, editable=False, null=True,
                                 verbose_name=_("Added date"),
                                 help_text=_("Added date"))

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'perms'
        ordering = ('name',)
        verbose_name = _("obligation")
        verbose_name_plural = _("obligations")


class Period(models.Model):
    REPORT_PERIOD = (
        ('annual', _("Annually")),
        ('semester', _("Semester")),
        ('quarter', _("Quarter"))
    )
    name = models.CharField(max_length=100, default='n/a',
                            verbose_name=_("Period name"),
                            help_text=_("Period name"))
    oblig = models.ForeignKey(Obligation, related_name='period_obligativity', blank=False, null=False, db_index=True,
                              on_delete=models.DO_NOTHING,
                              verbose_name=_("Obligativity"),
                              help_text=_("Period obligativity"))
    period = models.CharField(max_length=100, default='annual', choices=REPORT_PERIOD,
                              verbose_name=_("Report period"),
                              help_text=_("Report period"))

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'perms'
        ordering = ('name',)
        verbose_name = _("period")
        verbose_name_plural = _("period")


class Report(models.Model):
    name = models.CharField(max_length=100, default='n/a',
                            verbose_name=_("Report name"),
                            help_text=_("Report name"))
    org = models.ForeignKey(Org, related_name='org_report', blank=False, null=False, db_index=True,
                            on_delete=models.DO_NOTHING,
                            verbose_name=_("Organisation report"),
                            help_text=_("Organisation report"))
    oblig = models.ForeignKey(Obligation, related_name='org_oblig_report', blank=False, null=False, db_index=True,
                              on_delete=models.DO_NOTHING,
                              verbose_name=_("Organisation obligativity"),
                              help_text=_("Organisation obligativity"))
    period = models.ForeignKey(Period, related_name='org_period_report', blank=False, null=False, db_index=True,
                               on_delete=models.DO_NOTHING,
                               verbose_name=_("Organisation obligativity period"),
                               help_text=_("Organisation obligativity period"))
    is_completed = models.BooleanField(default=False, verbose_name=_("Is completed"))
    added = models.DateTimeField(auto_now_add=True, editable=False,
                                 verbose_name=_("Added date"),
                                 help_text=_("Added date"))
    modified = models.DateTimeField(auto_now=True, editable=False, null=True,
                                    verbose_name=_("Modified date"),
                                    help_text=_("Modified date"))

    def save(self, *args, **kwargs):
        super(Report, self).save(*args, **kwargs)
        secretary_change_report_by_country(self.org.country, self)
        secretary_view_report_by_country(self.org.country, self)
        commissar_view_report(self)

    def __unicode__(self):
        return self.name

    class Meta:
        permissions = (
            ('view_report', 'View report'),
        )
        app_label = 'perms'
        ordering = ('name',)
        verbose_name = _("report")
        verbose_name_plural = _("reports")
