from __future__ import unicode_literals

from django.contrib.auth.models import Group

from guardian.shortcuts import assign_perm


def secretary_view_report_by_country(country, obj):
    """Give view permission, for secretary, over a report
    base on secretary country"""
    g = 'Secretary_{}'.format(country.upper())
    try:
        group = Group.objects.get(name=g)
    except Group.DoesNotExist:
        raise Exception("Group {} was not found".format(g))
    assign_perm('view_report', group, obj)


def secretary_change_report_by_country(country, obj):
    """Give view permission, for secretary, over a report
    base on secretary country"""
    g = 'Secretary_{}'.format(country.upper())
    try:
        group = Group.objects.get(name=g)
    except Group.DoesNotExist:
        raise Exception("Group {} was not found".format(g))
    assign_perm('change_report', group, obj)


def commissar_view_report(obj):
    """A comissar can view all reports"""
    try:
        group = Group.objects.get(name='Commissar')
    except Group.DoesNotExist:
        raise Exception("Group {} was not found".format('Commissar'))
    assign_perm('view_report', group, obj)
