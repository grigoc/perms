from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import get_objects_for_user

from perms.models import Obligation, Org, Period, Report


@admin.register(Org)
class OrgAdmin(admin.ModelAdmin):
    list_display = ('name', 'country')


@admin.register(Obligation)
class ObligationAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Period)
class PeriodAdmin(admin.ModelAdmin):
    list_display = ('name', 'period')


@admin.register(Report)
class ReportAdmin(GuardedModelAdmin):
    list_display = ('name', 'org', 'org_country', 'oblig', 'period', 'is_completed')

    def org_country(self, obj):
        return obj.org.get_country_display()
    org_country.short_description = _("Country")

    def get_queryset(self, request):
        """Filter the queryset by permissions"""
        qs = super(ReportAdmin, self).get_queryset(request)
        return get_objects_for_user(request.user, 'view_report', klass=qs)
